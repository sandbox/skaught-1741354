# HTML Sample Module

The HTML Sample module is for testing/previewing theme renders of elements common 
in most Drupal sites, including many basic drupal interface freatures (tabs, 
messages, help) and Form API elements.

## Installing HTML Sample:

 * Place this complete directory in a Drupal module path, best: sites/all/modules/
 * Navigate to the modules administration page and enable the module.
 * Navigate to the permissions page and configure permissions. 
   [for non user/1 users]

## Using HTML Sample

 * Navigate to: http://example.com/html_sample and you will
   see a module in action. 
 * With permissions set, a user can the Select the theme.
